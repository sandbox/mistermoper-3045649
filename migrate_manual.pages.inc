<?php

/**
 * @file
 * Migrate ui extras pages.
 */

/**
 * Form to manually migrate elements.
 */
function migrate_manual_form($form, &$form_state) {

  $form['migration_name'] = array(
    '#title' => t('Migration'),
    '#type'  => 'select',
    '#options' => migrate_manual_form_migration_list(),
    '#required'    => TRUE,
  );

  $form['idlist'] = array(
    '#title' => t('ID list'),
    '#description' => t('Id of elements to migrate. You can separate by comma or lines.'),
    '#required'    => TRUE,
    '#type'        => 'textarea',
  );

  $form['force'] = array(
    '#title' => t('Force elements already migrated.'),
    '#type'  => 'checkbox',
  );

  // @TODO: validate elements exists in migration?
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Migrate'),
  );
  return $form;
}

/**
 * Form submit: migrate all elements.
 */
function migrate_manual_form_submit(&$form, &$form_state) {

  // Remove whitespaces and break lines from id list:
  $idlist = str_replace("\r\n", ',', $form_state['values']['idlist']);
  $idlist = preg_replace('/\s+/', '', $idlist);

  $options = array(
    'idlist' => $idlist,
    'force'  => $form_state['values']['force'],
  );

  $migration = Migration::getInstance($form_state['values']['migration_name']);
  $result = $migration->processImport($options);

  // Reset form if there are fails.
  $form_state['rebuild'] = $result == Migration::RESULT_FAILED;
}
